<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/documents/{id}', 'DocumentController@getDocuments');
Route::get('/user/certificate/{id}', 'DocumentController@getCertificate');
Route::get('/user/sof/{id}', 'DocumentController@getSof');
Route::get('/user/schedule/{id}/{version}', 'DocumentController@getSchedule');
Route::get('/user/document-historic/{uuid}', 'DocumentController@getHistoricDoc');
