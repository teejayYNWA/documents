@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Documents</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        {{--<document-card--}}
                        {{--doc-url="/user/documents/{id}"--}}
                        {{--doc-name="Statement of Fact"--}}
                        {{-->--}}
                        {{--</document-card>--}}

                        {{--<document-card--}}
                                {{--doc-url="/user/document2/{id}"--}}
                                {{--doc-name="Certificate"--}}
                        {{-->--}}
                        {{--</document-card>--}}
                        @if($user->user_type === 'superuser')
                        @foreach($user->documents as $document)

                            <document-card
                                    {{$document->document_name}}
                                    doc-url="/user/{{$document->document_name}}/{{$document->uuid}}/{{$document->version}}"
                                    doc-name="{{$document->document_name}}/{{$document->created_at->format('d-m-Y')}}"
                            >
                            </document-card>
                        <br>

                        @endforeach
                        @else

                        @endif


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
