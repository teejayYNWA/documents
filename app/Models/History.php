<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 12/07/18
 * Time: 13:41
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class History extends Model
{
    protected $table = 'tbl_history';
    protected $primaryKey = 'id';

    protected $fillable =[
        'snapshot_date', 'user_id', 'json_record'
    ];

    public function documents()
    {
        return $this->belongsTo(Document::class);
    }
}