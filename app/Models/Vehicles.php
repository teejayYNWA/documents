<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 12/07/18
 * Time: 13:40
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Vehicles extends Model
{
    protected $table = 'tbl_vehicles';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'market_value', 'insurance_group', 'engine_size',
        'make', 'model', 'year', 'reg_no',
        'registered_owner', 'main_user',
        'mileage', 'alarm', 'body_type',
    ];

}