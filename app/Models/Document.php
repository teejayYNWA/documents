<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 20/07/18
 * Time: 13:48
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'hidden'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function history()
    {
        return $this->hasOne(History::class, 'id', 'history_id');
    }
}