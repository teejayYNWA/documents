<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id', false, true);
            $table->string('reg_no', 8);
            $table->string('make');
            $table->string('model');
            $table->string('body_type');
            $table->integer('engine_size');
            $table->integer('year', false, true);
            $table->integer('market_value', false, true)->nullable();
            $table->integer('insurance_group', false, true);
            $table->integer('mileage', false, true)->nullable();
            $table->enum('main_user', ['yes', 'no']);
            $table->enum('registered_owner', ['yes', 'no']);
            $table->integer('alarm')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('tbl_vehicles');
    }
}
