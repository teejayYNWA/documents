<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 11/07/18
 * Time: 13:49
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller

{

    public function getCertificate()
    {
        $data = ['user' => User::with(['documents', 'documents.history'])->find(Auth::user()->id)];

        $pdf = \App::make('dompdf.wrapper');
        $exampleDoc = view('documents.certificate', $data)->render();

        $pdf->loadHTML($exampleDoc);

        return $pdf->stream();

    }

    public function getSof()
    {
        $data = ['user' => User::with(['documents', 'documents.history'])->find(Auth::user()->id)];

        $pdf = \App::make('dompdf.wrapper');
        $exampleDoc = view('documents.sof', $data)->render();

        $pdf->loadHTML($exampleDoc);

        return $pdf->stream();
    }

    public function getSchedule($version)
    {
        $data = ['user' => User::with(['documents', 'documents.history'])->find(Auth::user()->id)];

        $pdf = \App::make('dompdf.wrapper');
        $exampleDoc = view('documents.schedule'.$version, $data)->render();

        $pdf->loadHTML($exampleDoc);

        return $pdf->stream();
    }

}